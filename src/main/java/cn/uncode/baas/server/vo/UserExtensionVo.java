package cn.uncode.baas.server.vo;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import cn.uncode.dal.utils.JsonUtils;

public class UserExtensionVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4463521142918762403L;
	
	public static final String LOGIN = "login";
	public static final String SIGNUP = "signup";
	public static final String UPDATE = "update";
	
	//{"login":"...","signup":"...","update":"..."}
	private String login;
	private String signup;
	private String update;
	
	private MethodVo loginVo;
	private MethodVo signupVo;
	private MethodVo updateVo;
	
	public static UserExtensionVo valueOf(String value){
		if(StringUtils.isNotBlank(value)){
			Map<?, ?> tabMap = JsonUtils.fromJson(value, Map.class);
			if(tabMap != null){
				return UserExtensionVo.valueOf(tabMap);
			}
		}
		return null;
	}
	
	public static UserExtensionVo valueOf(Map<?,?> tabMap){
		if(tabMap != null){
			UserExtensionVo vo = new UserExtensionVo();
			if(tabMap.containsKey(LOGIN)){
				String my = String.valueOf(tabMap.get(LOGIN));
				if(StringUtils.isNotEmpty(my)){
					vo.setLoginVo(MethodVo.valueOf(my));
				}
			}
			if(tabMap.containsKey(SIGNUP)){
				String my = String.valueOf(tabMap.get(SIGNUP));
				if(StringUtils.isNotEmpty(my)){
					vo.setSignupVo(MethodVo.valueOf(my));
				}
			}
			if(tabMap.containsKey(UPDATE)){
				String my = String.valueOf(tabMap.get(UPDATE));
				if(StringUtils.isNotEmpty(my)){
					vo.setUpdateVo(MethodVo.valueOf(my));
				}
			}
			return vo;
		
		}
		return null;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSignup() {
		return signup;
	}

	public void setSignup(String signup) {
		this.signup = signup;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	public MethodVo getLoginVo() {
		return loginVo;
	}

	public void setLoginVo(MethodVo loginVo) {
		this.loginVo = loginVo;
	}

	public MethodVo getSignupVo() {
		return signupVo;
	}

	public void setSignupVo(MethodVo signupVo) {
		this.signupVo = signupVo;
	}

	public MethodVo getUpdateVo() {
		return updateVo;
	}

	public void setUpdateVo(MethodVo updateVo) {
		this.updateVo = updateVo;
	}
	
	
	

}
